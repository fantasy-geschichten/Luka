# Intro: Das Leben im Dorf

## Der letzte Abend mit Bernd

Ich sitze mit meinem Freund Bernd am See.
Die Sonne ist bereits untergegangen und wir unterhalten uns.
Wir waren zuvor darin baden, wie immer ganz nackig, und haben uns seitdem immer noch nicht angezogen.

Eigentlich sollten wir schon lange wieder zurück sein. Sven macht sich immer Sorgen, wenn wir so spät zurückkommen.
Aber heute ist eben eine Ausnahmesituation.
Mein letzter Tag hier im Dorf soll etwas besonderes sein.

Wir sind beide etwas traurig und wollen eigentlich gar nicht, dass dieser Tag endet.
Dabei war es meine eigene Idee, zu gehen.
Auch wenn es hier schön ist, würde ich eben gern mal etwas anderes sehen.
Und mein Ziel, das Schloss, ist ein beliebter Ort, wo es viel zu erleben gibt.

Bernd hat mich bei den Planungen gut unterstützt, auch wenn er anfangs nicht davon begeistert war.
Dass er jetzt so traurig sein würde, hätte ich gar nicht erwartet.
Er ist sonst immer gut drauf und verbringt auch oft genug Zeit alleine.

Aber heute Abend möchte er mich für keine Sekunde verlassen.
Er kommt mir sehr nahe, und kuschelt sich an mich.
Ich finde das etwas seltsam, wir sind ja beide Jungs und auch noch nackig.
Aber es gefällt mir auch irgendwie. Sein glattes, dunkelrotes Haar und sein kurviger Körper lassen ihn fast wie ein Mädel wirken.
Wenn ich nicht ständig seinen großen Pimmel im Blick hätte, wäre das jetzt richtig romantisch.

## Mein Freund Bernd

Wir hatten schon viele schöne Zeiten zusammen.
Schon als ich hier in der Wohngemeinschaft angekommen bin, hat sich Bernd gut um mich gekümmert.
Wir wurden schnell Freunde und haben seitdem den Großteil unserer Freizeit miteinander verbracht.
Und da im Dorf nicht viel zu tun ist, außer gelegentlich mal bei der Ernte zu helfen oder handwerkliche Kleinigkeiten, ist das eine lange Zeit.

Dass Bernd gleich so großes Interesse an mir gezeigt hat, hat mich überrascht. Ich bin immer ziemlich ruhig und habe keine besonderen Eigenschaften.
Ich bin nicht besonders stark, groß oder hübsch.
Ich relativ mager und meine braunen, kurzen Haare sind immer durcheinander.

Ich war daher wirklich froh, so schnell so einen guten Freund gefunden zu haben.
Dennoch würde ich das gerne ändern und eine interessantere Person werden.
Auch ein Grund, das Dorf zu verlassen.

Aber nun ist es wirklich spät.
Ich finde, wir sollten zurück zu unserer Wohnung gehen.
Bernd umarmt mich nochmal lange und fest, es ist mir schon unangenehm, aber dann ziehen wir uns endlich wieder an und gehen los, zurück zu unserer Wohnung.

## Alltag im Dorf

Meine anderen Mitbewohner sind auch nett.
Sven ist der älteste und auch größte von uns und passt immer gut auf alle auf. Er ist schlank und richtig durchtrainiert.
Er achtet auch darauf, dass wir genug Bewegung bekommen.
Nur Frederik zu motivieren gelingt ihn nicht oft.

Frederik ist auch ein richtig Süßer. Er ist sogar kleiner als ich, aber auch etwas moppelig. Er liegt viel herum und macht mit uns gerne Späße. Gerne erzählt er irgendwelchen Unsinn.
Aber ohne ihn wäre es daheim wohl oft etwas ruhig.

Beim Rückweg kommen wir am Haus der beiden Mädels Selia und Lena, mit denen wir oft Zeit verbringen.
Lena ist ziemlich süß. Sie ist klein, hat rotbraunes Haar und kocht immer gut.
Ich versteh mich mit ihr gut, auch wenn wir leider nicht viel zusammen unternehmen.
Die anderen scheinen sie aber zu ignorieren. Sie hält sich auch immer sehr zurück.
Frederik und Sven achten immer nur auf ihre Freundin Selia. Diese sieht richtig weiblich aus und steht auch gerne im Mittelpunkt.

## Besuch bei den Mädels

Als wir an dem Haus vorbeilaufen, meint Bernd, dass ich doch Lena nochmals besuchen soll, um mich richtig zu verabschieden.
Ihm ist wohl klar, wie sehr ich sie mag.
Ich möchte sie nicht stören, weil es schon spät ist, aber Bernd lässt nicht davon ab und kann mich dazu überreden, wie immer, wenn er etwas wirklich will.

Er umarmt mich nochmal und wünscht mir schon einmal eine gute Nacht, für den Fall dass es länger dauert.
Für eine halbe Stunde wird er sowieso warten, schon allein um sicherzugehen, dass ich keinen Rückzieher mache. Ich kann mir nicht vorstellen, dass es länger dauert.

Dann gehe ich zu ihrem Haus und klopfe an.
Es dauert etwas, aber dann macht Selia auf.
Sie trägt bereits ein Nachthemd. Darin schaut sie noch besser aus als sonst.
Mir wäre Lena auf jeden Fall lieber gewesen. Sven und Frederik würden sich über diesen Anblick bestimmt sehr freuen.

Selia bittet mich herein. Sie freut sich, dass ich auch mal allein komme.
Dass ich sie so sehe, stört sie offenbar nicht.

Selia möchte gar nicht wissen, was ich so spät hier will. Sie bietet mir sofort etwas zu trinken an und schlägt vor, dass ich doch bei ihr übernachte.
Das kommt unerwartet aber freut mich auch.
Ich erkläre ihr, dass ich mich nur noch persönlich bei ihnen verabschieden wollte, insbesondere bei Lena, bevor ich gehe.
Selia wusste noch nichts von meiner Reise.
Leider schläft Lena bereits.

Also verabschiede ich mich zumindest von Selia und mache mich bereit, zu gehen.
Doch Selia macht mir klar, dass es wirklich kein Problem wäre, wenn ich bei ihr übernachte.
Das ist ja heute die letzte Gelegenheit.
Sie hat auch /genügend/ Schlafkleidung für mich dabei.
Sie lacht mich dabei an, als würde sie etwas vor mir verheimlichen.

Ich folge ihr erstmal in ihr Zimmer.
Auf einmal steht Lena hinter uns.
Sie wundert sich, was hier los ist, und warum ich da bin.
Als ich ihr davon erzähle, meint sie, dass sie schon von meiner Reise gehört hat, und fängt fast an, zu weinen.
Ich nehme sie in den Arm und streichle ihr über den Rücken.

Selia meint, ich soll doch mit Lena in ihr Zimmer gehen.
Lena geht voraus und ich ihr nach.
Selia sieht mir nach und scheint gemischte Gefühle zu haben.

Bei Lena setze ich mich zu ihr ins Bett. Sie hat sich zwar schon etwas beruhigt, aber ich versuche dennoch, sie zu trösten.
Ich erzähle ihr, dass ich mit ihr immer gern zusammen war. Vermutlich war ihr das bewusst, aber das mal zu hören hat sie dennoch sehr erfreut.

Ich leg mich zu ihr ins Bett und drücke sie fest. Ich sehe ihr direkt ins Gesicht und sie sieht glücklich aus.
Als sie so direkt vor mir liegt, gebe ich ihr einfach einen Kuss als wäre es völlig normal. Es kommt mir angebracht vor.
Sie reagiert nicht darauf, aber sie sieht immer noch genauso glücklich aus, wie vorher.

Nach wenigen Minuten macht sie die Augen zu und schläft ein. Ich überlege, ob ich gehen soll, entscheide mich dann aber, liegen zu bleiben.

## Der Abschied

Als ich am nächsten Morgen aufwache, ist Lena nicht mehr im Bett.
Ich muss sofort aufstehen. Es wird eine lange Reise.
Ich verabschiede mich nochmal von Lena, und auch Selia, und gehe dann nach Hause, wo Sven und Frederik schon auf mich warten.
Sie fragen nicht, wo ich letzte Nacht war, und ich bin auch froh darüber, auch wenn sie es demnächst vermutlich sowieso erfahren werden.

Bernd ist gerade nicht aufzufinden. Vermutlich fällt ihm der Abschied doch etwas schwerer als geglaubt.
Ich habe leider keine Zeit, ihn zu suchen oder auf ihn zu warten.
Aber zumindest von Frederik und Sven verabschiede ich mich noch lange.
Dann hole ich mein Gepäck, das ich am Vortag schon zusammengepackt hatte, dann geht es endlich los zum Schloss.

# Kapitel 1: Die Reise ins Ungewisse

# Kapitel 2: Die Amulette der Magier

# Kapitel 3: Die weite Welt

# Kapitel 4: Die wahre Macht der Amulette

# Kapitel 5: Lehren der Anderen

# Kapitel 6: König der Magie

# Kapitel 7: Freundschaft, Rückzug und Familie

# Kapitel 8: Die Kinder

# Kapitel 9: Terror

# Kapitel 10: Was passiert ist

# Kapitel 11: Der größte aller Magier

# Kapitel 12: Der Anfang vom Ende

